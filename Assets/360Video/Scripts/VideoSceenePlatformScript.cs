﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoSceenePlatformScript : MonoBehaviour
{
    [SerializeField] VideoHolderScriptableObject videoHolder;

	private void Awake()
	{
		gameObject.transform.position = videoHolder.getActivePlatform().gameObject.transform.position;
	}

	private void OnTriggerExit(Collider other)
	{
		//1. Unhide objects in original sceene
		GameObject[] activeSceenRootParetns = SceneManager.GetSceneByBuildIndex(videoHolder.getActiveSceneIndex()).GetRootGameObjects();
		foreach (GameObject rootParent in activeSceenRootParetns)
		{
			rootParent.SetActive(true);
		}
		//2. reset skybox
		videoHolder.applyDefaultSkybox();

		//3. reactivate trigger of original platform
		videoHolder.getActivePlatform().GetComponent<VideoPlatformScript>().reactivatePlatformTrigger();

		//3. destroy this
		Destroy(this.gameObject);
	}
}
