﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class VideoPlatformScript : MonoBehaviour
{
	private string videoPlayerScene = "VideoScene";

	[SerializeField] private VideoClip videoClip;
    [SerializeField] private VideoHolderScriptableObject videoHolder;
	[SerializeField] private GameObject videoExitPlatform;

	private GameObject player;
    private Scene ActiveSceeneOtsideOfVideo;

	//Possible hack! alter the skybox of main sceene instead, 
	//that way we avoid lagg and keep things simple.
	private void OnTriggerEnter(Collider other)
	{
		//1. prevent further colitions
		GetComponent<CapsuleCollider>().enabled = false;
		//2. update videoHolder
		videoHolder.setActiveSceeneIndex(SceneManager.GetActiveScene().buildIndex);
		videoHolder.setVideoToPlay(videoClip);
		videoHolder.setActivePlatform(this.gameObject);
		//3. set skybox to video
		videoHolder.applyVideoTextureToSkybox();
		//4 hide everything in sceene exept potantially the player and teleporting prefab (given not recomended setup)

		List<Scene> scenes = videoHolder.getActiveScenens();

		foreach (Scene scene in scenes)
		{
			GameObject[] objectsInScene = scene.GetRootGameObjects();

			foreach (GameObject rootParent in objectsInScene)
			{
				if (rootParent.name == "Teleporting" || rootParent.name == "Player")
				{

				}
				else
				{
					rootParent.SetActive(false);
				}
			}
		}

		//5. Create video Platform instacne
		Instantiate(videoExitPlatform);
	}

	private void OnTriggerExit(Collider other)
	{
		
	}
    
	public void reactivatePlatformTrigger()
	{
		GetComponent<CapsuleCollider>().enabled = true;
	}

}
