﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class videoSceeneScript : MonoBehaviour
{
    [SerializeField] private VideoHolderScriptableObject videoHolder;
    private VideoPlayer videoPlayer;

	//triggers when videoscene has loaded. playing the video only when everything else is done. 
    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.clip = videoHolder.getVideoToPlay();
        videoPlayer.Play();
    }

	//TODO: give user more options. aka, auto ending videos and so on and forth. 
}
