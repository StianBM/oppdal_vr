﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

[CreateAssetMenu]
public class VideoHolderScriptableObject: ScriptableObject
{

    private VideoClip videoToPlay;
	private GameObject activePlatform;
    private int activeSceeneIndex;
	private List<Scene> loadedScenes;

	[SerializeField] Material defaultSkyboxMaterial;
	[SerializeField] Material videoPlayerSkyboxMaterial;

	private void Awake()
	{
		loadedScenes = new List<Scene>();
		videoToPlay = null;
		activePlatform = null;
	}

	private void OnEnable()
	{
		SceneManager.sceneLoaded += onSceneLoaded;
		SceneManager.sceneUnloaded += onSceneUnloaded;
	}

	private void OnDisable()
	{
		loadedScenes = new List<Scene>();
		SceneManager.sceneLoaded += onSceneLoaded;
		SceneManager.sceneUnloaded += onSceneUnloaded;
	}

	//Adds Scenes to list of sceenes to help hide objects
	private void onSceneUnloaded(Scene newScene)
	{
		if (loadedScenes.Contains(newScene))
		{
			loadedScenes.Remove(newScene);
		}
	}

	private void onSceneLoaded(Scene newScene, LoadSceneMode mode)
	{
		loadedScenes.Add(newScene);
	}

	public List<Scene> getActiveScenens()
	{
		return loadedScenes;
	}

	//adds and removes video renderer to active sceene skybox
	public void applyVideoTextureToSkybox()
	{
		RenderSettings.skybox = videoPlayerSkyboxMaterial;
	}

	public void applyDefaultSkybox()
	{
		RenderSettings.skybox = defaultSkyboxMaterial;
	}

	//sets the videoplayers Video to clipp stored in this componetn
	public void setVideoToPlay(VideoClip newVideoToPlay)
    {
        videoToPlay = newVideoToPlay;
    } 

    public VideoClip getVideoToPlay()
    {
        return videoToPlay;
    }
	
	//sets the active platform (the platform the players tepped on)
	public void setActivePlatform(GameObject newActivePlatform)
	{
		activePlatform = newActivePlatform;
	}

	public GameObject getActivePlatform()
	{
		return activePlatform;
	}

	//stores a refferance to the active sceene. 
    public void setActiveSceeneIndex(int newIndex)
    {
        activeSceeneIndex = newIndex;
    }

    public int getActiveSceneIndex()
    {
        return activeSceeneIndex;
    }

}
