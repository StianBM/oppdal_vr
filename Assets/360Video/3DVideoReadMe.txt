IMTEL VR-LABB NTNU, Virituell praksis. 

----Preparation-----

1. Video Spesifications
	a. Videos need to be compatable with unity, (max resolutin ~ 5k) import the videos into yout project, and look out for errors.
	When done corectly you should be able to prewiew the videos in the inspector.
	b. The videos needs to be stiched to the "over under"(or "left eye over") format. 
	c. Be carfull with video length. The videofiles are large and should not be longer than they need to. Edit them carefully. 

----Instructions-----

1. Import A 3D Video
	a. Drag your desierd 360 overunder 3D video into your project. I recomend creating a folder called 3DVideoClips. 
	b. You will need to create or update the "2.5KRenderTexture" texture in the 360Video folder(the asset you imported) to fit the dimentions of your video.
		I. The default dimetions are 2560 (Terrible resolution), you want somwere around 5k.
		II. Click your imported video in the project view to find its dimentions.
		III. Click the "2.5KRenderTextrue" in the project view.
		IV. Change the "Size" field in the inspector to match your videos aspect ratio.
		V. Rename the rendertexture to match your spesified resolution.
		
		VI. (optional) You might have more than one resolution on your different videos.
		This is not supported by this version of the 360Video component. Creating logic
		for swapping out the RenderTexture in the videop player in the videoScene is
		howver not hard. I recomend scaling the videos to the same resolution if possible
		and making this logic only if no other options are avalable. 

2. Adding The VideoPlatrofm
	a. The "360VideoPlatform" is the prefab responsible for displaying video to a unser.
	b. Dragg an instance of this prefab into your sceene.
	c. Click the instance in your hirarcy to view it in the inspector. 
	d. Note that the "Video Platform Script" instance on this prefab is missing a Video Clip
	e. That is were you need to drag a refferance to the videoclip prepared in 1.
	f. (optional) repeat this prosess for any number of videos. rememer to keep the aspectratio consistent with the rendertexture(see above)

3. Layers
	a. This version of the 360 Video component uses layers to hide the rest of the sceene when a video is playing.
	b. A layer needs to be added to your project, name it "360Video" or alter the name used in "VideoPlatforScript"
	c. Setting the "Teleporting" prefab in your scene to this new "360Video" layer, will give users some teleporting feedback while wathcing video.
	d. (optional) The Video platform comes with a teleport plane attached, however, using the teleport planes that are already in your project
		will prevent players from teleporting into walls ect. when they exit the video. Simply add your teleport planes to the vissible layer (360Video), and you will
		be able to see them while in the video. 

4. NonDefaultSkybox
	a. if your project uses a non-default skybox, this skybox needs to be put as the "default skybox" on the "SkyboxHolder" scriptable object.
	b. Logic might be needed to determine witch skybox to use, if the project has multiple different skyboxes. 
 



