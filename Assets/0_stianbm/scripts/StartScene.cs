﻿/* Component that can load the next scene. Can also set the omniMovement flag of RuntimeVariables component in __app.
 */

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class StartScene : MonoBehaviour
{
    public UnityEvent startScene;

    [SerializeField]
    private string nextScene;

    private GameObject __app;

    // Start is called before the first frame update
    public void Start()
    {
        startScene.Invoke();

        __app = GameObject.Find("__app");

        if (__app == null)
        {
            Debug.LogError("[StartScene] __app not found");
        }
    }

    public void OmniEnabled()
    {
        __app.GetComponent<RuntimeVariables>().omniMovement = true;
    }

    public void OmniDisabled()
    {
        __app.GetComponent<RuntimeVariables>().omniMovement = false;
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(nextScene);
    }
}
