﻿/* Teleports the player to the targetTeleporter
 */


using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField]
    private Transform targetTeleporter;

    [SerializeField]
    private Transform player;

    public void Teleport()
    {
        player.position = new Vector3(targetTeleporter.position.x, targetTeleporter.position.y + 1, targetTeleporter.position.z);
        player.rotation = targetTeleporter.rotation;
    }
}
