﻿/* Instantiates prefabs and assigns them parents. Also checks if the prefab already exists in the parent, in which case it isn't spawned. Also
 * sets the layer of the instance to the layer of the prefab.
 */


using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [Tooltip("Object to be spawned")]
    [SerializeField]
    private List<GameObject> objectReferences;

    [Tooltip("Parent for object (for easier destruction)")]
    [SerializeField]
    private List<Transform> parents;

    public void SpawnObjects()
    {
        for(int i = 0; i < objectReferences.Count;i++)
        {
            if (!GameObject.Find(parents[i].name + "/" + objectReferences[i].name + "(Clone)"))
            {
                GameObject clone = Instantiate(objectReferences[i], parents[i]);
                clone.layer = objectReferences[i].gameObject.layer;
            }
        }
    }
}
