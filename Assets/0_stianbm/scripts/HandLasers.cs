﻿/* Component that can activate and deactivate the SteamVRLaserpointers. It does this by toggling the laserActive flag and toggling the
 * laserLayer on the culling mask of the Camera.
 */


using System.Collections.Generic;
using UnityEngine;
using Valve.VR.Extras;


public class HandLasers : MonoBehaviour
{
    [SerializeField]
    private int laserLayer = 9;

    [SerializeField]
    private Camera vrCamera;

    [Tooltip("Parent of the two hands")]
    [SerializeField]
    private Transform SteamVRObjects;


    private Transform leftHand;

    private Transform rightHand;

    private List<Transform> hands;

    
    private void Awake()
    {
        leftHand = SteamVRObjects.Find("LeftHand");
        rightHand = SteamVRObjects.Find("RightHand");
    }

    public void ActivateHandLasers()
    {
        leftHand.GetComponent<SteamVR_LaserPointerCustom>().lasersActive = true;
        rightHand.GetComponent<SteamVR_LaserPointerCustom>().lasersActive = true;
        vrCamera.cullingMask |= 1 << laserLayer;
    }

    public void DeactivateHandLasers()
    {
        leftHand.GetComponent<SteamVR_LaserPointerCustom>().lasersActive = false;
        rightHand.GetComponent<SteamVR_LaserPointerCustom>().lasersActive = false;
        vrCamera.cullingMask &= ~(1 << laserLayer);
    }
}
