﻿/* Adds custom handling to events raised by the SteamVRLaserPointers. Set up to run the Action() function of the clicked target's
 * LaserClickAction component.
 */


using UnityEngine;
using Valve.VR.Extras;

public class SteamVRLaserWrapperCustom : SteamVRLaserWrapper
{
    protected new void Awake()
    {
        base.Awake();
        if(steamVrLaserPointer != null)
        {
            Debug.Log("[SteamVRLaserWrapperCustom] SteamVRLaserPointer found");
        }
        else
        {
            Debug.Log("[SteamVRLaserWrapperCustom] SteamVRLaserPointer NOT found");
        }
    }

    protected override void OnPointerClick(object sender, PointerEventArgs e)
	{
		Debug.Log("[LaserWrapper] " + "Clicked");
		Debug.Log("[LaserWrapper] " + "target: " + e.target.name);
		if (e.target.tag == "PointArea")
		{
			Debug.Log("[LaserWrapper] " + "clicked on pointable");
			e.target.GetComponent<LaserClickAction>().Action();
		}
		else if(e.target == null)
		{
			Debug.Log("[LaserWrapper]	no target clicked");
		}
		else
		{
			Debug.Log("[LaserWrapper]	unknown target clicked");
		}
	}
}
