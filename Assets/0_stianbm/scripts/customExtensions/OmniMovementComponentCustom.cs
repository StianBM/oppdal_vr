﻿/* Adds a flag for whether or not movement is enabled. If movement is not enabled the GetOmniInputForCharacterMovement() update returns after
 * setting velocities to zero. It also inhibits Omni from requesting more packages in Update(). 
 */


using UnityEngine;

public class OmniMovementComponentCustom : OmniMovementComponent
{
	// Added
	[Header("-----custom-----")]
	public bool movementEnabled = true;

	public override void GetOmniInputForCharacterMovement()
	{
		if (cameraReference == null)
		{
			Debug.LogError("OmniMovementComponent(GetOmniInputForCharacterMovement) - Camera Reference not set in prefab.");
			return;
		}

		forwardMovement = new Vector3(0.0f, 0.0f, 0.0f);
		strafeMovement = forwardMovement;

		// Added
		if (!movementEnabled)
		{
			return;
		}

		//calculate angle between camera and omni
		angleBetweenOmniAndCamera = ComputeAngleBetweenControllerAndCamera();

		float forwardRotation = 0f;

		//keep within bounds (0, 360)
		if (currentOmniYaw > 360f) currentOmniYaw -= 360f;
		if (currentOmniYaw < 0f) currentOmniYaw += 360f;

		//Get the coupling percentage from System
		if (hasSetCouplingPercentage == false)
		{
			couplingPercentage = developerMode ? 1.0f : OmniMovementCalibration.GetCouplingPercentage();
			hasSetCouplingPercentage = true;
		}

		//calculate forward rotation
		forwardRotation = (currentOmniYaw - omniOffset + transform.rotation.eulerAngles.y) + (angleBetweenOmniAndCamera * couplingPercentage);

		//display forward rotation defined by our coupling percentage
		dummyObject.rotation = Quaternion.Euler(0, forwardRotation, 0);
		dummyForward = dummyObject.forward;

		//calculate forward movement
		Vector3 movementInputVector = new Vector3(hidInput.y * dummyForward.x, 0, hidInput.y * dummyForward.z);

		//apply multiplier to reduce backwards movement speed by a given percentage
		if (hidInput.y < 0) { movementInputVector *= backwardsSpeedMultiplier; }

		//display rounded values
		if (dummyForward.x != 0)
			dummyForward.x = Mathf.Round(dummyForward.x * 100) / 100;
		if (dummyForward.z != 0)
			dummyForward.z = Mathf.Round(dummyForward.z * 100) / 100;

		//apply gravity
		movementInputVector += Physics.gravity * gravityMultiplier;
		//perform forward movement

		
		forwardMovement = (movementInputVector * maxSpeed * Time.deltaTime);

		//check if there is strafe input
		if (hidInput.x != 0)
		{
			//calculate strafe movement
			movementInputVector = new Vector3(hidInput.x * dummyObject.right.x, 0, hidInput.x * dummyObject.right.z);
			//apply modifier to reduce strafe movement by a given percentage
			movementInputVector *= strafeSpeedMultiplier;
			//apply gravity
			movementInputVector += Physics.gravity * gravityMultiplier;
			//perform strafe movement
			strafeMovement = (movementInputVector * maxSpeed * Time.deltaTime);
		}
	}

	public override void Update()
	{
		if (!developerMode && movementEnabled)
		{
			ReadOmniData();
			CalibrateOmni();
		}
	}
}
