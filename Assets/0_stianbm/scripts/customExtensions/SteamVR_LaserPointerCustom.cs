﻿/* Adds a layer to check for in the raycast in order to point through invisible objects. Also adds a flag for activating and deactivating the
 * laser. Now it also adds the spawned laserpointer to a specific layer.
 */


using UnityEngine;

namespace Valve.VR.Extras
{
	public class SteamVR_LaserPointerCustom : SteamVR_LaserPointer
	{
		[SerializeField]
		private int laserLayer = 9;

		[SerializeField]
		private int pointableLayer = 10;

        public bool lasersActive = true;
        

        protected override void Start()
		{
			if (pose == null)
				pose = this.GetComponent<SteamVR_Behaviour_Pose>();
			if (pose == null)
				Debug.LogError("No SteamVR_Behaviour_Pose component found on this object");

			if (interactWithUI == null)
				Debug.LogError("No ui interaction action has been set on this component.");


			holder = new GameObject();
			holder.transform.parent = this.transform;
			holder.transform.localPosition = Vector3.zero;
			holder.transform.localRotation = Quaternion.identity;

			pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
			pointer.transform.parent = holder.transform;
			pointer.transform.localScale = new Vector3(thickness, thickness, 100f);
			pointer.transform.localPosition = new Vector3(0f, 0f, 50f);
			pointer.transform.localRotation = Quaternion.identity;
			BoxCollider collider = pointer.GetComponent<BoxCollider>();
			if (addRigidBody)
			{
				if (collider)
				{
					collider.isTrigger = true;
				}
				Rigidbody rigidBody = pointer.AddComponent<Rigidbody>();
				rigidBody.isKinematic = true;
			}
			else
			{
				if (collider)
				{
					Object.Destroy(collider);
				}
			}
			Material newMaterial = new Material(Shader.Find("Unlit/Color"));
			newMaterial.SetColor("_Color", color);
			pointer.GetComponent<MeshRenderer>().material = newMaterial;

			pointer.layer = laserLayer;		// Added
		}
       
        protected override void Update()
		{
            if (lasersActive)
            {
                if (!isActive)
                {
                    isActive = true;
                    this.transform.GetChild(0).gameObject.SetActive(true);
                }

                float dist = 100f;

                Ray raycast = new Ray(transform.position, transform.forward);
                RaycastHit hit;
                bool bHit = Physics.Raycast(raycast, out hit, Mathf.Infinity, (1 << pointableLayer));       // Changed

                if (previousContact && previousContact != hit.transform)
                {
                    PointerEventArgs args = new PointerEventArgs();
                    args.fromInputSource = pose.inputSource;
                    args.distance = 0f;
                    args.flags = 0;
                    args.target = previousContact;
                    OnPointerOut(args);
                    previousContact = null;
                }
                if (bHit && previousContact != hit.transform)
                {
                    PointerEventArgs argsIn = new PointerEventArgs();
                    argsIn.fromInputSource = pose.inputSource;
                    argsIn.distance = hit.distance;
                    argsIn.flags = 0;
                    argsIn.target = hit.transform;
                    OnPointerIn(argsIn);
                    previousContact = hit.transform;
                }
                if (!bHit)
                {
                    previousContact = null;
                }
                if (bHit && hit.distance < 100f)
                {
                    dist = hit.distance;
                }

                if (bHit && interactWithUI.GetStateUp(pose.inputSource))
                {
                    PointerEventArgs argsClick = new PointerEventArgs();
                    argsClick.fromInputSource = pose.inputSource;
                    argsClick.distance = hit.distance;
                    argsClick.flags = 0;
                    argsClick.target = hit.transform;
                    OnPointerClick(argsClick);
                }

                if (interactWithUI != null && interactWithUI.GetState(pose.inputSource))
                {
                    pointer.transform.localScale = new Vector3(thickness * 5f, thickness * 5f, dist);
                    pointer.GetComponent<MeshRenderer>().material.color = clickColor;
                }
                else
                {
                    pointer.transform.localScale = new Vector3(thickness, thickness, dist);
                    pointer.GetComponent<MeshRenderer>().material.color = color;
                }
                pointer.transform.localPosition = new Vector3(0f, 0f, dist / 2f);
            }
        }
	}	
}
