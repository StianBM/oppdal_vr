﻿/* Component that can either destroy all children in the linked content transform, or destroy the transform alltogether.
 */


using UnityEngine;

public class DestroyContent : MonoBehaviour
{
    [Tooltip("Child where spawned objects, like highlighters and textboxes, are stored to be destroyed when exiting image mode")]
    [SerializeField]
    private Transform content;

    public void DestroyGameobjectsInContent()
    {
        foreach (Transform child in content)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
    
    public void DestroyObject()
    {
        GameObject.Destroy(content.gameObject);
    }
}
