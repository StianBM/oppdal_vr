﻿/* Static function that can be used all over the project.
 */



using UnityEngine;

public static class OppdalResources : object
{
    // Sets the layer of a transform and all it's children.
	public static void SetLayerRecursively(Transform transform, int layer)
	{
		transform.gameObject.layer = layer;

		for (int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild(i);
			SetLayerRecursively(child, layer);
		}
	}
}
