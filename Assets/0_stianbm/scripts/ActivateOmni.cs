﻿/* Component that either activates Omni and deactivates Teleportating, or the other way around. Reads a flag in the RutimeVariables component
 * of the __app gameobject. The flag is set in the menu scene.
 */


using UnityEngine;

public class ActivateOmni : MonoBehaviour
{
    private GameObject __app;
    private GameObject teleporting;

    // Start is called before the first frame update
    void Start()
    {
        __app = GameObject.Find("__app");

        if(__app == null)
        {
            Debug.LogError("[ActivateOmni] __app not found");
        }

        bool enabled = __app.GetComponent<RuntimeVariables>().omniMovement;

        Debug.Log("ActivateOmni] enabled: " + __app.GetComponent<RuntimeVariables>().omniMovement);

        gameObject.GetComponent<CharacterController>().enabled = enabled;
        gameObject.GetComponent<OmniController_Example>().enabled = enabled;
        gameObject.GetComponent<SetOmniConnectGamepadMode>().enabled = enabled;
        gameObject.GetComponent<OmniMovementComponentCustom>().enabled = enabled;

        teleporting = GameObject.Find("Teleporting");

        if (teleporting == null)
        {
            Debug.LogError("[ActivateOmni] Teleporting not found");
        }
        else
        {
            teleporting.SetActive(!enabled);
        }
    }
}
