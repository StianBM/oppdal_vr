﻿/* Shows different models for a landslide by activating and deactivating them. Does checks as not to run all the time. Needs the number of
 * landslides and the models must be in correct order.
 */


using UnityEngine;
using UnityEngine.Events;

public class LandslideHandler : MonoBehaviour
{
    [SerializeField]
    private Valve.VR.InteractionSystem.LinearMapping linearMapping;

    [Tooltip("Transform where landslide models are kept")]
    [SerializeField]
    private Transform landslides;

    [Tooltip("Number of landslides")]
    [SerializeField]
    private int nLandslides = 0;

    public UnityEvent changeLandslideModel;

    
    private float partitionSize = 0.0f;
    private Transform currentLandslide = null;

    // Optimizing
    private float currentLinearMapping = float.NaN;
    private int previousIndex = 0;


    void Awake()
    {
        if (linearMapping == null)
        {
            Debug.LogError("[LandslideHandler] Warning, linearMapping not referenced");
        }

        partitionSize = 1.0f/nLandslides;

        Debug.Log("[LandslideHandler] nLandslides: " + nLandslides);
        Debug.Log("[LandslideHandler] partitionSize: " + partitionSize);

        currentLandslide = landslides.GetChild(0);
    }

    void Update()
    {
        if(currentLinearMapping != linearMapping.value)
        {
            currentLinearMapping = linearMapping.value;

            int index = FindIndex();

            if(index != previousIndex)
            {
                previousIndex = index;

                Debug.Log("[LandslideHandler] index: " + index);
                Debug.Log("[LandslideHandler] currentLinearMapping: " + currentLinearMapping);

                changeLandslideModel.Invoke();

                //Instantiate(landslides[index], transform);

                currentLandslide.gameObject.SetActive(false);
                currentLandslide = landslides.GetChild(index);
                currentLandslide.gameObject.SetActive(true);
            }
        }
    }

    private int FindIndex()
    {
        int index = 0;
        float value = currentLinearMapping;

        while(value > partitionSize)
        {
            value -= partitionSize;
            index++;
        }

        return index;
    }
}
