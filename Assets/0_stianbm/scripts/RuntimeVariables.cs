﻿/* Runtime variables component to be attached to __app and therefore be available in all scenes as it isn't destroyed on load.
 */


using UnityEngine;

public class RuntimeVariables : MonoBehaviour
{
    public bool omniMovement = true;

    public void Start()
    {
        omniMovement = false;
    }
}
