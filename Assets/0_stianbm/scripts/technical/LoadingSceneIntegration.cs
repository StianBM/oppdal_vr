﻿/* Used for debugging by launching the active scene after loading up the _preload scene with an object that isn't destroyed on load.
 */

// Based on kolodi at stack overflow
// https://stackoverflow.com/questions/35890932/unity-game-manager-script-works-only-one-time/35891919#35891919


using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneIntegration
{

#if UNITY_EDITOR
	public static int otherScene = -2;

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
	static void InitLoadingScene()
	{ 
		Debug.Log("[LoadingSceneIntegration]	InitLoadingScene()");
		int sceneIndex = SceneManager.GetActiveScene().buildIndex;
		if (sceneIndex == 0) return;

		Debug.Log("[LoadingSceneIntegration]	Loading _preload scene");
		otherScene = sceneIndex;
		//make sure your _preload scene is the first in scene build list
		SceneManager.LoadScene(0);
	}
#endif
}
