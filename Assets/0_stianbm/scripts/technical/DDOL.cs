﻿/* Component that creates an object that isn't destroyed. Used in the __preload scene to load the Menu scene afterwards. Comented part is used
 * With LoadingSceneIntergration for debugging purposes. More info in LoadingSceneIntegration.cs.
 */


using UnityEngine;
using UnityEngine.SceneManagement;

public class DDOL : MonoBehaviour
{
	public void Awake()
	{
		DontDestroyOnLoad(gameObject);
		Debug.Log("[DDOL]	" + gameObject.name);

        /*
		if (LoadingSceneIntegration.otherScene > 0)
		{
			SceneManager.LoadScene(LoadingSceneIntegration.otherScene);
		}
		else
		{
			SceneManager.LoadScene("Menu");
		}
        */

        SceneManager.LoadScene("Menu");
    }
}
