﻿/* Activates and deactivates linked objects.
 */


using System.Collections.Generic;
using UnityEngine;


public class ObjectActivater : MonoBehaviour
{
    [Tooltip("Objects to be activated and deactivated together")]
    [SerializeField]
    private List<GameObject> objectReferences;

    public void activateObjects()
    {
        foreach (GameObject thing in objectReferences){
            thing.SetActive(true);
        }
    }

    public void deactivateObjects()
    {
        foreach(GameObject thing in objectReferences)
        {
            thing.SetActive(false);
        }
    }
}
