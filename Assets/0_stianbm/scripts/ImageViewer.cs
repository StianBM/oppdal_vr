﻿/* Allows the player to view a 360 degree stereoscopic image. Does this by changing the the material of the skybox and changing the culling
 * mask of the camera. Also deactivates movement, both omni and teleporting, when entering image mode.
 */


using UnityEngine;
using UnityEngine.Events;


namespace Valve.VR.InteractionSystem.Sample
{
    public class ImageViewer : MonoBehaviour
    {
        #region variables

        [Header("-----Player-----")]

		[Tooltip("To modify culling masks to enable image mode")]
		[SerializeField]
        private Camera vrCamera;

		[Tooltip("To modify culling masks to enable image mode")]
		[SerializeField]
        private Camera debugCamera;

		[Tooltip("To disable movement when in image mode")]
		[SerializeField]
		private GameObject player;

		[Header("-----Own Components-----")]

		[Tooltip("To add listener and change layer to make visible in image mode")]
		[SerializeField]
        private HoverButton hoverButton;

		[Header("-----Layering-----")]

		[SerializeField]
        private LayerMask imageLayerMask;

        [SerializeField]
        private LayerMask defaultLayerMask;

        [SerializeField]
        private int defaultLayer = 0;

        [SerializeField]
        int activeInImageLayer = 8;

		[Header("-----Image-----")]

		[SerializeField]
		private Material imageMaterial;

		private Material defaultSkyboxMaterial;

        [Header("-----Events-----")]

        public UnityEvent enterImage;
        public UnityEvent exitImage;

        private GameObject teleporting = null;

        #endregion variables

        private void Start()
        {
            defaultSkyboxMaterial = RenderSettings.skybox;
            Debug.Log("[ImageViewer]	extracted default skybox texture");

            // Returns null if Teleporting is deactivated (Omni is active)
            teleporting = GameObject.Find("Teleporting");
            if(teleporting == null)
            {
                Debug.Log("[ImageViewer] teleporting not found");
            }
        }

        public void ToggleImage()
        {
            // Enter image mode
            if(RenderSettings.skybox == defaultSkyboxMaterial)
            {
                SetSkyboxMaterialAndLayer(imageMaterial, activeInImageLayer, imageLayerMask);
                player.GetComponent<OmniMovementComponentCustom>().movementEnabled = false;

                SetTeleportingActiveFlag(false);

                enterImage.Invoke();
            }

            // Exit image mode
            else if (RenderSettings.skybox == imageMaterial)
            {
                SetSkyboxMaterialAndLayer(defaultSkyboxMaterial, defaultLayer, defaultLayerMask);
                player.GetComponent<OmniMovementComponentCustom>().movementEnabled = true;

                SetTeleportingActiveFlag(true);

                exitImage.Invoke();
            }

            else
            {
                Debug.LogError("ImageViewer:	unexpected skybox material");
            }

            Debug.Log("[ImageViewer]    current layermask applied:	" + imageLayerMask.ToString());
            Debug.Log("[ImageViewer]	current material applied:	" + RenderSettings.skybox.ToString());
            Debug.Log("[ImageViewer]    hoverbutton layer set to:   " + hoverButton.gameObject.layer.ToString());
        }

        private void SetSkyboxMaterialAndLayer(Material skyboxMaterial, int layer, LayerMask layerMask)
        {
            RenderSettings.skybox = skyboxMaterial;

            SetImageViewerVisibilityLayers(layer);

            vrCamera.cullingMask = layerMask;
            if (debugCamera)
            {
                debugCamera.cullingMask = layerMask;
            }
        }

		// Changes the layer of the visible components (button and cylinder) to make it visible in image mode
		private void SetImageViewerVisibilityLayers(int layer)
		{
			Transform button = hoverButton.transform;
			OppdalResources.SetLayerRecursively(button, layer);

			Transform cylinder = transform.GetChild(1);
			OppdalResources.SetLayerRecursively(cylinder, layer);
		}

        // Checks if 
        private void SetTeleportingActiveFlag(bool value)
        {
            if(teleporting != null)
            {
                teleporting.SetActive(value);
                Debug.Log("[ImageViewer] teleporting set to: " + value);
            }
        }
    }
}
