﻿/* Script that exits the application
 */

using UnityEngine;


public class ExitApplication : MonoBehaviour
{
   public void ExitApplicationAction()
    {
        Application.Quit();
    }
}
