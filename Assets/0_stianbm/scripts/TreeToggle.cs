﻿/* Component that can toggle transforms on and off.
 */


using UnityEngine;

public class TreeToggle : MonoBehaviour
{
    [Tooltip("Transform that holds the tree objects")]
    [SerializeField]
    private Transform trees;

    private bool treesAreActive = true;

    public void ToggleTrees()
    {
        treesAreActive = !treesAreActive;

        trees.gameObject.SetActive(treesAreActive);
    }
}
