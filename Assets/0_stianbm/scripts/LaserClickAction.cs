﻿/* Component attached to an object that can be pointed at and clicked by the hand lasers. When clicked, the Action() function is called.
 */


using UnityEngine;
using UnityEngine.Events;

public class LaserClickAction : MonoBehaviour
{
	[SerializeField]
	public GameObject highlighter = null;

    [SerializeField]
    private bool highlighterEnabled = true;

    [SerializeField]
    public Transform content;

    public UnityEvent clicked;


	public void Action()
	{
		// Spawn highlighter
		// Check if the child already exists and if prefab is linked
		if (!GameObject.Find(content.name + "/" + highlighter.name + "(Clone)") && highlighter != null && highlighterEnabled)
		{
			GameObject highlighterInstance = Instantiate(highlighter, transform.position, transform.rotation, content);
			highlighterInstance.transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}

        clicked.Invoke();
    }
}
