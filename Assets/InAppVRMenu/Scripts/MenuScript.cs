﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using UnityEngine.UI;

namespace Valve.VR.InteractionSystem
{
    public class MenuScript : MonoBehaviour
    {
        public SteamVR_Action_Boolean menuButton;
        Camera PlayerCamaera;
        private bool toggled = false;
        [SerializeField] private GameObject panel;
        [SerializeField] private float MenuSpawnDistance;
        public Transform folowHead;

        private void OnEnable()
        {
            if (menuButton == null)
            {
                Debug.LogError("<b>[SteamVR Interaction]</b> No MenuButton action assigned, Have you updated steamVR inputsources and rebound controllers, see documnetation for VirituellPraksis standard menu");
                return;
            }
            menuButton.AddOnStateDownListener(HandleMenuPress, SteamVR_Input_Sources.Any);
        }

        private void OnDisable()
        {
            if (menuButton != null)
                menuButton.RemoveOnStateDownListener(HandleMenuPress, SteamVR_Input_Sources.Any);
        }

        private void HandleMenuPress(SteamVR_Action_Boolean actionIn, SteamVR_Input_Sources inputSource)
        {
            toggleMenu();
        }
   
        public void toggleMenu()
        {
            this.gameObject.transform.position = folowHead.forward * MenuSpawnDistance + folowHead.position;
            this.gameObject.transform.rotation = folowHead.rotation;

            toggled = !toggled;
            panel.SetActive(toggled);
            pauseGame(toggled);
        }

        public void pauseGame(bool toggled)     //TODO: Gray out background
        {
            if (toggled)
            { 
                Time.timeScale = 0.01f;
            } else
            {
                Time.timeScale = 1f;
            }
        }
    }
}