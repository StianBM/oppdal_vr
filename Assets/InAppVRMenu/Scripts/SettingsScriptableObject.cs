﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SettingsScriptableObject : ScriptableObject
{

    [SerializeField] private float volume;

    public void setVolume(float newVolume)
    {
        volume = newVolume;
    }

    public float getVolume()
    {
        return volume;
    }


}
