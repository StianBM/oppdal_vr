﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using UnityEngine.SceneManagement;

public class ApplicationMenuButtonsScript : MonoBehaviour
{
    [SerializeField] private SettingsScriptableObject settings;

    private string hubLocation;

    private void Awake()
    {
        hubLocation = Path.Combine(Application.dataPath, "/..//../hub/Virituell Praksis Katalog.exe");
    }

    public void ReloadScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ExitApplication()
    {
        Application.Quit();
    }

    public void ReturnToHub()
    {
        FileInfo hubappinfo = new FileInfo(hubLocation);
        if (hubappinfo.Exists)
        {
            Process.Start(new ProcessStartInfo(hubappinfo.FullName) { WorkingDirectory = hubappinfo.DirectoryName });
            Process.GetCurrentProcess().Kill();
        } else
        {
            Debug.Log("finner ikke hub");
        }
    }

}
