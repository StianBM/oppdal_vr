﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeButtonScript : MonoBehaviour
{

    private VolumeSliderScript vs;
    private float value;

    public void onClick()
    {
        vs.settVolume(value);
    }

    public void setVolumeValue(float newValue)
    {
        value = newValue;
    }

    public float getVolumeValue()
    {
        return value;
    }

    public void setVolumeSliderScript(VolumeSliderScript newvs)
    {
        vs = newvs;
    }

}
