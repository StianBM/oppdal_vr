﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSliderScript : MonoBehaviour
{
    [SerializeField] private GameObject VolumeButton;
    [SerializeField] private int numberOfButtons;
    [SerializeField] private SettingsScriptableObject settings;
    [SerializeField] private Color[] buttonColors;

    private GameObject[] arrayOfVolumeButtons;
    private float currentVolume;

    private void Awake()
    {
        currentVolume = settings.getVolume();
        AudioListener.volume = currentVolume;
    }

    private void Start()
    {
        arrayOfVolumeButtons = new GameObject[numberOfButtons];
        for (int i = 0; i < arrayOfVolumeButtons.Length; i++)
        {
            arrayOfVolumeButtons[i] = Instantiate(VolumeButton, this.transform);
            arrayOfVolumeButtons[i].GetComponent<VolumeButtonScript>().setVolumeValue((float)i / (float)numberOfButtons);
            arrayOfVolumeButtons[i].GetComponent<VolumeButtonScript>().setVolumeSliderScript(this);
        }

        updateButtonColors();
    }

    public void settVolume(float value) //TODO: check if value is within accaptable volume range
    {
        currentVolume = value;
        settings.setVolume(value);
        AudioListener.volume = value;
        updateButtonColors();
    }

    private void updateButtonColors()
    {
        foreach (GameObject button in arrayOfVolumeButtons)
        {
            VolumeButtonScript bs = button.GetComponent<VolumeButtonScript>();
            if (bs.getVolumeValue() <= currentVolume)
            {
                button.GetComponent<Button>().image.color = buttonColors[0];
            }
            else
            {
                button.GetComponent<Button>().image.color = buttonColors[1];
            }
        }
    }
}
