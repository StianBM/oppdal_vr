# the_oppdal_project

Virtual field trip of Vekveselva in Oppdal.
The application is meant as preparation before attending the actual field trip. It combines stereoscopic 360 images with a ground model recreated from laser scans.

## Prereqruisites
Unity 2018.4.12f1
HTC Vive Pro 2
Virtuix Omni

## Installing
Clone project into a local repository
NB! Username might be necessary to added (without brackets) to the link:
`https://[username]@gitlab.stud.idi.ntnu.no/stianbm/the_oppdal_project.git`

All dependencies like SteamVR and OmniSDK are included in the project.

## Usage
Open the "Oppdal" scene and start from Unity.

## License
Oh boy, who knows?
